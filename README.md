# PostgreSQL base deploy 

"Quick, ugly and dirty" scripts to deploy and configure a PostgreSQL server.

Work in progress...

## Informations

* OS : GNU Linux Debian

## Goal

* deploy VM on Open Nebula cluster
* configure PostgreSQL repository
* install PostgreSQL packages
* configure DB and users

## Requirements

### Python modules
* [pyone](https://pypi.org/project/pyone/)
* [click](https://pypi.org/project/click/) (for python script use)
* [tabulate](https://pypi.org/project/tabulate/) (for python script use)

### Ansible
* [ONE VM module in Community General Collection](https://docs.ansible.com/ansible/latest/collections/community/general/one_vm_module.html#ansible-collections-community-general-one-vm-module)
* [OpenNebula inventory in Community General Collection](https://docs.ansible.com/ansible/latest/collections/community/general/opennebula_inventory.html)
* ["OpenNebula VMs deploy Ansible role"](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/one-vms-deploy)
* ["PostgreSQL install" Ansible role](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-install)
* ["PostgreSQL operations" Ansible role](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-ops)

## Usage

### Shell script

**Note :** Shell script will be deprecated soon. It was written to elaborate the proof of concept.

To display csv file content :

```
bash pgbase-deploy.sh readcsv csvfilename.csv
```

To start deployment :

```
bash pgbase-deploy.sh deploy csvfilename.csv
```

### Python script


**Note :** it's an early alpha release.

To display csv file content :

```
python pgbase-deploy.py readcsv --csvfile csvfilename.csv
```

To start deployment :

```
python pgbase-deploy.py deploy --csvfile csvfilename.csv
```
