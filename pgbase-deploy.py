#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = ["Stephane PAILLET"]
__date__ = "2023-09-27"
__license__ = "GNU GPL v3"
__status__ = "Development"
__version__ = "0.1.0alpha"

import click
import csv
from os import getcwd as pwd
from os.path import isfile
from subprocess import run
from tabulate import tabulate

# Vars
scriptpath = pwd()
inventorydir = "inventory"
deployuser = "root"
limit = "db"

@click.group()
@click.version_option(__version__)
def cli():
    """Script to deploy Postgresql in basic install mode"""
    pass

@click.command()
@click.option('--csvfile', help='csv filename')
def readcsv(csvfile):
    """Read and display CSV file"""
    if isfile('{}/{}'.format(scriptpath, csvfile)):
        try:
            data = load_csv(csvfile)
            head = ['Hostname', 'Label', 'vCPU', 'RAM', 'Disque']
            table_content = []
            for row in data:
                table_content.append([row['hostname'], row['label'], 
                row['vcpu'], row['ram'] + 'GB', row['disk'] + 'GB'])
            click.echo(tabulate(table_content, headers = head, tablefmt="grid"))
        except ValueError:
            click.echo('Something goes wrong')
    else:
        click.echo('No csv selected')

@click.command()
@click.option('--csvfile', help='csv filename')
@click.password_option()
def deploy(csvfile, password):
    """Deploy VMs and PostgreSQL instance"""
    if isfile('{}/{}'.format(scriptpath, csvfile)):
        set_vault(password)
        import_roles()
        deploy_vms(csvfile)
        create_inventory()
        install_pgsql()
    else:
        click.echo('No csv selected')

def load_csv(csvfile):
    """Read csv and load it in a list of dicts"""
    with open('{}/{}'.format(scriptpath, csvfile)) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=['hostname', 
                                'label', 'vcpu', 'ram', 'disk'])
        next(reader)
        data = list(reader)
        return data

def set_vault(password):
	vaultfile = open('{}/.vault'.format(scriptpath), 'w')
	vaultfile.write(password)

def import_roles():
    """Import Ansible roles"""
    try:
        run(['ansible-galaxy', 
             'install', '-f', '-r', 
             '{}/roles/requirements.yml'.format(scriptpath)])
    except IOError:
        click.echo('Something goes wrong, Ansible role import aborted')

def deploy_vms(csvfile):
    """Deploy VMs"""
    try:
        data = load_csv(csvfile)
        for row in data:
            run(['ansible-playbook', '-e', 
                 'one_vm_name="{}" one_vm_labels="{}"\
                 one_vm_vcpu="{}"\
                 one_vm_memory="{} GB"\
                 one_vm_disk_size="{} GB"'.format(row['hostname'], 
                 row['label'], row['vcpu'], row['ram'], 
                 row['disk']), '--vault-password-file=.vault', 
                 '{}/one-vms-deploy.yml'.format(scriptpath)])
    except IOError:
        click.echo('Something goes wrong, VMs deployment aborted')

def create_inventory():
    """Create Ansible inventory"""
    try:	
        run(['ansible-inventory', '-y', '--list', '--output', 
             '{}/{}/hosts'.format(scriptpath, inventorydir), '-i', 
             '{}/{}/inventory-opennebula.yml'.format(scriptpath, inventorydir)])
        click.echo('Ansible inventory created')
    except IOError:
        click.echo('Something goes wrong, Ansible inventory creation aborted')

def install_pgsql():
    """Install PostgreSQL instance"""
    try:
        run(['ansible-playbook', 
             '{}/pgsqlbase-deploy.yml'.format(scriptpath), 
             '-i', '{}/{}/hosts'.format(scriptpath, inventorydir), 
             '-e', 'ansible_user="{}" pgsql_op=dbcreate'.format(deployuser), 
             '--vault-password-file=.vault', '--limit', limit])
    except IOError:
        click.echo('Something goes wrong, PostgreSQL instance install aborted')

cli.add_command(readcsv)
cli.add_command(deploy)

if __name__ == "__main__":
    cli()
