#!/usr/bin/env bash
# PGSL base
# Deploy PostgreSQL VM
# Author: Stephane Paillet
# Date: 2023-09-27

# Vars
scriptpath=$(pwd)
inventorydir='inventory'
deployuser='root'
limit='db'
vmslist=$2

function DISPLAY_USAGE {
echo "Usage : $(basename $0) [-h] CSVfile action
Quickly and ugly script :)
where:
   -h  	   : show help command
   CSVfile : file to parse
   action  : readcsv or deploy"
}

# Function to read the CSV content
function READCSV {
  cat ${vmslist} | sed 1d |
  while IFS=',' read vmname group vcpu ram disk
    do
      echo "name: ${vmname} group: ${group} vcpu: ${vcpu} memory: ${ram} GB disk size: ${disk} GB"
   done
}

# Function to launch VMs deployement ansible playbook
function SETVAULTPASS {
  # Load vault password
  read -s -p "Enter vault password:" vault_passwd
  echo ${vault_passwd} > .vault
  echo -ne "\r"
}

function CLONEROLES {
  ansible-galaxy install -f -r ${scriptpath}/roles/requirements.yml
}

# Function to launch VMs deployement ansible playbook
function VMSDEPLOY {

  cat ${vmslist} | sed 1d |
  while IFS=',' read vmname group vcpu ram disk
    do
      ansible-playbook -e "one_vm_name='${vmname}' one_vm_labels='${group}' \
      one_vm_vcpu='${vcpu}' one_vm_memory='${ram} GB' \
      one_vm_disk_size='${disk} GB'" \
      --vault-password-file=.vault ${scriptpath}/one-vms-deploy.yml
    done
}

function CREATEINV {
  ansible-inventory -y --list --output ${scriptpath}/${inventorydir}/hosts -i ${scriptpath}/${inventorydir}/inventory-opennebula.yml
}

# Function to launch VMs deployement ansible playbook
function PGINSTALL {
  ansible-playbook ${scriptpath}/pgsqlbase-deploy.yml -i ${scriptpath}/${inventorydir}/hosts -e "ansible_user='${deployuser}' pgsql_op=dbcreate" --vault-password-file=.vault --limit ${limit}
}

# Check Usage
[[ $# -ne 2 ]] && DISPLAY_USAGE && exit 1
[[ "$1" == "-h" ]] && DISPLAY_USAGE && exit 0

# Main
if [ $1 == readcsv ]; then
  READCSV
elif [ $1 == deploy ]; then
  SETVAULTPASS
  CLONEROLES
  VMSDEPLOY
  CREATEINV
  PGINSTALL
else DISPLAY_USAGE
fi
